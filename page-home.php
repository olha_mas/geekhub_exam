<?php
/**
 * Template Name: Homepage Template 1
 */

get_header(); ?>

<section class="about-us">
    <div class="container">
        <div class="col-md-6">
            <h2 class="text-uppercase">About us </h2>
            <p class="sub-title"><i>Our Short Story</i></p>
        </div>
        <div class="col-md-6 right-block-aboutus">
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
            <button type="button" class="btn btn-warning read-btn text-uppercase">Read more</button>
        </div>
    </div>
</section>


<?php

    $args = array( 'post_type' => 'service', 'posts_per_page' => 4 ); // show 3 posts only
    $loop = new WP_Query( $args );
    
    if ( $loop->have_posts() ):
?>

<section class="services">
    <div class="container border">
        <h2 class="text-uppercase">Services</h2>
        <p class="sub-title"><i>What we are doing</i></p>
        <ul class="services-items no-padding-left">
            
            <?php
                while ( $loop->have_posts() ) : $loop->the_post(); 
            ?>            
            
                <li class="col-md-6">
                    <div class="icon-block flex-container">
                        <i class="icon boosting-your-business-icon"></i>
                    </div>
                    <h3><?php the_title(); ?></h3>
                    <p><?php the_content(); ?></p>
                </li>
            
            <?php endwhile; ?>

        </ul>

        <button type="button" onclick="location.href = '/service/'; " class="btn btn-warning read-btn text-uppercase text-center">View more</button>

        <div class="pull-right">

            <div class="triangle triangle-yellow">
                <div class="triangle triangle-green">

                </div>
            </div>
        </div>
    </div>
</section>

<?php

    endif;
    
?>


<?php

    $args = array( 'post_type' => 'client', 'posts_per_page' => 3 ); // show 3 posts only
    $loop = new WP_Query( $args );
    
    if ( $loop->have_posts() ):
?>

<section class="clients-section">
    <div class="container">
        <div class="row">
            <div class="col-md-12 heading-wrap">
                <h2> Clients </h2>
                <span>Whats our client says </span>

            </div>

            <div class="col-md-12">
                
                
                <?php
                

                while ( $loop->have_posts() ) : $loop->the_post(); 
                ?>
                
                    <div class="item1 grey-quote">
                        <p>
                            <?php the_content(); ?>
                        </p>

                        <div class="profile-wrap clearfix">
                            <figure>
                                <?php if ( has_post_thumbnail() ): ?>
                                    <?php the_post_thumbnail() ?>
                                <?php endif; ?>
                            </figure>
                            <div class="profile-cont">
                                <strong><?php the_title() ?></strong>
                                <?php if(get_post_meta(get_the_ID(), 'geekhub_exam_client_position', true)): ?>
                                <span><?php echo get_post_meta(get_the_ID(), 'geekhub_exam_client_position', true); ?></span>
                                <?php endif; ?>
                            </div>

                        </div>
                    </div>                
                
                <?php endwhile; ?>                
                

            </div>

        </div>
    </div>

</section>

<?php endif; ?> 

<section class="partners">
    <div class="container">
        <h2 class="text-uppercase">Partners</h2>
        <p class="sub-title text-uppercase"><i>Our Great Partners</i></p>
        <div class="row">
            <div class="border">
                <img src="<?php echo get_template_directory_uri(); ?>/img/partners1.png">
            </div>
            <div class="border">
                <img src="<?php echo get_template_directory_uri(); ?>/img/partners2.png">
            </div>
            <div class="border">
                <img src="<?php echo get_template_directory_uri(); ?>/img/partners3.png">
            </div>
            <div class="border">
                <img src="<?php echo get_template_directory_uri(); ?>/img/partners4.png">
            </div>
            <div class="border">
                <img src="<?php echo get_template_directory_uri(); ?>/img/partners5.png">
            </div>
            <div class="border">
                <img src="<?php echo get_template_directory_uri(); ?>/img/partners6.png">
            </div>
        </div>
    </div>

    <div class="pull-right">

        <div class="triangle triangle-yellow">
            <div class="triangle triangle-green">

            </div>
        </div>
    </div>

</section>
<div class="clearfix"></div>	

<?php
//get_sidebar();
get_footer();
