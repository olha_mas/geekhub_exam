<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package GeekHub_Exam_Theme
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class('container'); ?>>


    <div class="row">
        
   
        <div class="col-md-7">
            <header class="entry-header">
                <?php
                if (is_single()) {
                    the_title('<h1 class="entry-title text-uppercase">', '</h1>');
                } else {
                    the_title('<h2 class="entry-title text-uppercase"><a href="' . esc_url(get_permalink()) . '" rel="bookmark">', '</a></h2>');
                }

                if ('post' === get_post_type()) :
                    ?>
                    <div class="entry-meta">
                        <?php geekhub_exam_theme_posted_on(); ?>
                    </div><!-- .entry-meta -->
                    <?php endif;
                ?>
            </header><!-- .entry-header -->      
        </div>
    </div>



    <div class="entry-content">
        
        <?php if ( has_post_thumbnail() ): ?>
            <div class="post-thumb">
                <?php the_post_thumbnail('large') ?>
            </div>
        <?php endif; ?>           
        
        <?php
            if (is_single()) {
                
                the_content(sprintf(
                            /* translators: %s: Name of current post. */
                            wp_kses(__('Continue reading %s <span class="meta-nav">&rarr;</span>', 'geekhub-exam-theme'), array('span' => array('class' => array()))), the_title('<span class="screen-reader-text">"', '"</span>', false)
                        ));
            } else {
                        the_excerpt(sprintf(
                            /* translators: %s: Name of current post. */
                            wp_kses(__('Continue reading %s <span class="meta-nav">&rarr;</span>', 'geekhub-exam-theme'), array('span' => array('class' => array()))), the_title('<span class="screen-reader-text">"', '"</span>', false)
                        ));
        
            echo '<button type="button" onclick="location.href = \' '.  get_the_permalink().' \' " class="btn btn-warning read-btn text-uppercase">Read more</button>';

        }
        
        wp_link_pages(array(
            'before' => '<div class="page-links">' . esc_html__('Pages:', 'geekhub-exam-theme'),
            'after' => '</div>',
        ));
        
        
        ?>
    </div><!-- .entry-content -->

    <footer class="entry-footer">
        <?php // geekhub_exam_theme_entry_footer(); ?>
    </footer><!-- .entry-footer -->
</article><!-- #post-## -->
