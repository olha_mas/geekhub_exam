<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package GeekHub_Exam_Theme
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo('charset'); ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="profile" href="http://gmpg.org/xfn/11">
        <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

        <?php wp_head(); ?>
    </head>

    <body <?php body_class(); ?>>
        <div id="page" class="site">
            <a class="skip-link screen-reader-text" href="#main"><?php esc_html_e('Skip to content', 'geekhub-exam-theme'); ?></a>

            <header id="masthead" class="site-header container" role="banner">

                <nav class="navbar navbar-default text-uppercase">
                    <div class="container">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#primary-navbar-collapse" aria-expanded="false">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="<?php echo esc_url(home_url('/')); ?>">
                                <?php if ( get_theme_mod('logo')  ): ?>
                                    <img src="<?php echo get_theme_mod('logo') ?>">
                                <?php endif; ?>
                            </a>
                            <?php if ( get_theme_mod('geekhub_exam_theme_phone') ) : ?>
                            <a href="tel:<?php echo get_theme_mod('geekhub_exam_theme_phone'); ?>" class="phone">
                                <i class="icon-call-out"></i>
                                <?php echo get_theme_mod('geekhub_exam_theme_phone'); ?>
                            </a>
                            <?php endif; ?>
                        </div>

                        <div class="collapse pull-right navbar-collapse" id="primary-navbar-collapse">
                            <?php wp_nav_menu(array('theme_location' => 'primary', 'menu_id' => 'primary-menu', 'items_wrap' => '<ul class="nav navbar-nav">%3$s')); ?>
                        </div>
                    </div>
                </nav>
                
            </header><!-- #masthead -->

            <div id="content" class="site-content">

                <?php echo do_shortcode('[rev_slider homepage-slider]'); ?>