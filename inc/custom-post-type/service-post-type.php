<?php

add_action( 'init', 'geekhub_exam_service_init' );
/**
 * Register a book post type.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_post_type
 */
function geekhub_exam_service_init() {
	$labels = array(
		'name'               => _x( 'Services', 'post type general name', 'geekhub-exam-theme' ),
		'singular_name'      => _x( 'Service', 'post type singular name', 'geekhub-exam-theme' ),
		'menu_name'          => _x( 'Services', 'admin menu', 'geekhub-exam-theme' ),
		'name_admin_bar'     => _x( 'Service', 'add new on admin bar', 'geekhub-exam-theme' ),
		'add_new'            => _x( 'Add New', 'service', 'geekhub-exam-theme' ),
		'add_new_item'       => __( 'Add New Service', 'geekhub-exam-theme' ),
		'new_item'           => __( 'New Service', 'geekhub-exam-theme' ),
		'edit_item'          => __( 'Edit Service', 'geekhub-exam-theme' ),
		'view_item'          => __( 'View Service', 'geekhub-exam-theme' ),
		'all_items'          => __( 'All Services', 'geekhub-exam-theme' ),
		'search_items'       => __( 'Search Services', 'geekhub-exam-theme' ),
		'parent_item_colon'  => __( 'Parent Services:', 'geekhub-exam-theme' ),
		'not_found'          => __( 'No services found.', 'geekhub-exam-theme' ),
		'not_found_in_trash' => __( 'No services found in Trash.', 'geekhub-exam-theme' )
	);

	$args = array(
		'labels'             => $labels,
                'description'        => __( 'Description.', 'geekhub-exam-theme' ),
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'service' ),
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => null,
                'menu_icon'           => 'dashicons-analytics',
		'supports'           => array( 'title', 'editor', 'thumbnail', 'excerpt' )
	);

	register_post_type( 'service', $args );
}