<?php

add_action( 'init', 'geekhub_exam_client_init' );
/**
 * Register a book post type.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_post_type
 */
function geekhub_exam_client_init() {
	$labels = array(
		'name'               => _x( 'Clients', 'post type general name', 'geekhub-exam-theme' ),
		'singular_name'      => _x( 'Client', 'post type singular name', 'geekhub-exam-theme' ),
		'menu_name'          => _x( 'Clients', 'admin menu', 'geekhub-exam-theme' ),
		'name_admin_bar'     => _x( 'Client', 'add new on admin bar', 'geekhub-exam-theme' ),
		'add_new'            => _x( 'Add New', 'client', 'geekhub-exam-theme' ),
		'add_new_item'       => __( 'Add New Client', 'geekhub-exam-theme' ),
		'new_item'           => __( 'New Client', 'geekhub-exam-theme' ),
		'edit_item'          => __( 'Edit Client', 'geekhub-exam-theme' ),
		'view_item'          => __( 'View Client', 'geekhub-exam-theme' ),
		'all_items'          => __( 'All Clients', 'geekhub-exam-theme' ),
		'search_items'       => __( 'Search Clients', 'geekhub-exam-theme' ),
		'parent_item_colon'  => __( 'Parent Clients:', 'geekhub-exam-theme' ),
		'not_found'          => __( 'No clients found.', 'geekhub-exam-theme' ),
		'not_found_in_trash' => __( 'No clients found in Trash.', 'geekhub-exam-theme' )
	);

	$args = array(
		'labels'             => $labels,
                'description'        => __( 'Description.', 'geekhub-exam-theme' ),
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'client' ),
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => null,
                'menu_icon'           => 'dashicons-groups',
		'supports'           => array( 'title', 'editor', 'thumbnail', 'excerpt' )
	);

	register_post_type( 'client', $args );
}