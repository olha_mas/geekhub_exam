<?php

/**
 * GeekHub Exam Theme Theme Customizer.
 *
 * @package GeekHub_Exam_Theme
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function geekhub_exam_theme_customize_register($wp_customize) {
    $wp_customize->get_setting('blogname')->transport = 'postMessage';
    $wp_customize->get_setting('blogdescription')->transport = 'postMessage';
    $wp_customize->get_setting('header_textcolor')->transport = 'postMessage';


    // Custom settings here
    
    $wp_customize->add_section('geekhub_exam_theme_socials_links', array(
        'title' => __('Social links', 'geekhub_exam_theme'),
        'priority' => 30,
    ));
    //Facebook
    $wp_customize->add_setting('social_links_facebook', array(
        'default' => '',
        'transport' => 'refresh',
    ));

    $wp_customize->add_control(new WP_Customize_Control($wp_customize, 'social_links_facebook', array(
        'label' => __('Facebook', 'geekhub_exam_theme'),
        'section' => 'geekhub_exam_theme_socials_links',
        'settings' => 'social_links_facebook',
    )));

    //Twitter
    $wp_customize->add_setting('social_links_twitter', array(
        'default' => '',
        'transport' => 'refresh',
    ));

    $wp_customize->add_control(new WP_Customize_Control($wp_customize, 'social_links_twitter', array(
        'label' => __('Twitter', 'geekhub_exam_theme'),
        'section' => 'geekhub_exam_theme_socials_links',
        'settings' => 'social_links_twitter',
    )));

    //Google+
    $wp_customize->add_setting('social_links_google', array(
        'default' => '',
        'transport' => 'refresh',
    ));

    $wp_customize->add_control(new WP_Customize_Control($wp_customize, 'social_links_google', array(
        'label' => __('Google+', 'geekhub_exam_theme'),
        'section' => 'geekhub_exam_theme_socials_links',
        'settings' => 'social_links_google',
    )));

    //Youtube

    $wp_customize->add_setting('social_links_youtube', array(
        'default' => '',
        'transport' => 'refresh',
    ));

    $wp_customize->add_control(new WP_Customize_Control($wp_customize, 'social_links_youtube', array(
        'label' => __('Youtube', 'geekhub_exam_theme'),
        'section' => 'geekhub_exam_theme_socials_links',
        'settings' => 'social_links_youtube',
    )));

    //Linkedin

    $wp_customize->add_setting('social_links_linkedin', array(
        'default' => '',
        'transport' => 'refresh',
    ));

    $wp_customize->add_control(new WP_Customize_Control($wp_customize, 'social_links_linkedin', array(
        'label' => __('Linkedin', 'geekhub_exam_theme'),
        'section' => 'geekhub_exam_theme_socials_links',
        'settings' => 'social_links_linkedin',
    )));

    //Dribbble

    $wp_customize->add_setting('social_links_dribbble', array(
        'default' => '',
        'transport' => 'refresh',
    ));

    $wp_customize->add_control(new WP_Customize_Control($wp_customize, 'social_links_dribbble', array(
        'label' => __('Dribbble', 'geekhub_exam_theme'),
        'section' => 'geekhub_exam_theme_socials_links',
        'settings' => 'social_links_dribbble',
    )));
    
    // LOGO here
    
    // Section
    $wp_customize->add_section( 'logo_section' , array(
        'title'       => __( 'Logo', 'textdomain' ),
        'priority'    => 30,
        'description' => __( 'Upload a logo', 'textdomain' ),
    ) );

    // Logo
    $wp_customize->add_setting( 'logo' );

    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'logo', array(
        'label'    => __( 'Logo', 'geekhub-exam-theme' ),
        'section'  => 'logo_section',
        'settings' => 'logo',
    ) ) );  
    
    
    //Phone
    $wp_customize->add_section( 'phone_section' , array(
        'title'       => __( 'Phone', 'textdomain' ),
        'priority'    => 30,
        'description' => __( 'Phone', 'textdomain' ),
    ) );
    
    $wp_customize->add_setting('geekhub_exam_theme_phone', array(
        'default' => '',
        'transport' => 'refresh',
    ));

    $wp_customize->add_control(new WP_Customize_Control($wp_customize, 'geekhub_exam_theme_phone', array(
        'label' => __('Phone', 'geekhub-exam-themegeekhub-exam-theme'),
        'section' => 'phone_section',
        'settings' => 'geekhub_exam_theme_phone',
    )));    
    
}

add_action('customize_register', 'geekhub_exam_theme_customize_register');


/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function geekhub_exam_theme_customize_preview_js() {
    wp_enqueue_script('geekhub_exam_theme_customizer', get_template_directory_uri() . '/js/customizer.js', array('customize-preview'), '20151215', true);
}

add_action('customize_preview_init', 'geekhub_exam_theme_customize_preview_js');
