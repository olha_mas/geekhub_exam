<?php
/**
 * Include and setup custom metaboxes and fields. (make sure you copy this file to outside the CMB2 directory)
 *
 * @category GeekHubExam
 * @package  CMB2
 * @license  http://www.opensource.org/licenses/gpl-license.php GPL v2.0 (or later)
 * @link     https://github.com/WebDevStudios/CMB2
 */

/**
 * Get the bootstrap! If using the plugin from wordpress.org, REMOVE THIS!
 */

if ( file_exists( dirname( __FILE__ ) . '/cmb2/init.php' ) ) {
	require_once dirname( __FILE__ ) . '/cmb2/init.php';
} elseif ( file_exists( dirname( __FILE__ ) . '/CMB2/init.php' ) ) {
	require_once dirname( __FILE__ ) . '/CMB2/init.php';
}

add_action( 'cmb2_admin_init', 'geekhub_exam_register_client_page_metabox' );
/**
 * Hook in and add a metabox that only appears on the 'Client' page
 */
function geekhub_exam_register_client_page_metabox() {
	$prefix = 'geekhub_exam_client_';

	/**
	 * Metabox to be displayed on a single page ID
	 */
	$cmb_clients_page = new_cmb2_box( array(
		'id'           => $prefix . 'metabox',
		'title'        => __( 'Additional Info', 'cmb2' ),
		'object_types' => array( 'client', ), // Post type
		'context'      => 'normal',
		'priority'     => 'high',
		'show_names'   => true, // Show field names on the left
	) );

	$cmb_clients_page->add_field( array(
		'name' => __( 'Job Position', 'cmb2' ),
		'desc' => __( 'Job position text here (optional)', 'cmb2' ),
		'id'   => $prefix . 'position',
		'type' => 'text',
	) );

}


/**
 * Meta for Service post type
 */


add_action( 'cmb2_admin_init', 'geekhub_exam_register_service_page_metabox' );
/**
 * Hook in and add a metabox that only appears on the 'Client' page
 */
function geekhub_exam_register_service_page_metabox() {
	$prefix = 'geekhub_exam_service_';

	/**
	 * Metabox to be displayed on a single page ID
	 */
	$cmb_services_page = new_cmb2_box( array(
		'id'           => $prefix . 'metabox',
		'title'        => __( 'Additional Info', 'cmb2' ),
		'object_types' => array( 'service', ), // Post type
		'context'      => 'normal',
		'priority'     => 'high',
		'show_names'   => true, // Show field names on the left
	) );

	
        

}
