<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package GeekHub_Exam_Theme
 */
?>

</div><!-- #content -->


<footer class="site-footer" >
    <div class="container">
        <div class="col-md-3">
            <a class="navbar-brand" href="<?php echo esc_url(home_url('/')); ?>">
                <?php if (get_theme_mod('logo')): ?>
                    <img src="<?php echo get_theme_mod('logo') ?>">
                <?php endif; ?>                   
            </a>
            <div class="clearfix"></div>
            <p>2016©</p>
            <?php if (!empty(get_theme_mod('social_links_facebook'))) { ?>
                <a class="facebook" href="<?php echo get_theme_mod('social_links_facebook'); ?>"><span class="fa fa-facebook"></span></a>
            <?php } ?>   

            <?php if (!empty(get_theme_mod('social_links_twitter'))) { ?> 
                <a class="twitter" href="<?php echo get_theme_mod('social_links_twitter'); ?>"><span class="fa fa-twitter"></span></a>
            <?php } ?>

            <?php if (!empty(get_theme_mod('social_links_google'))) { ?>    
                <a class="google-plus" href="<?php echo get_theme_mod('social_links_google'); ?>"><span class="fa fa-google-plus"></span></a>
            <?php } ?>

            <?php if (!empty(get_theme_mod('social_links_youtube'))) { ?>      
                <a class="youtube" href="<?php echo get_theme_mod('social_links_youtube'); ?>"><span class="fa fa-youtube"></span></a>
            <?php } ?>    

            <?php if (!empty(get_theme_mod('social_links_linkedin'))) { ?>      
                <a class="linkedin" href="<?php echo get_theme_mod('social_links_linkedin'); ?>"><span class="fa fa-linkedin"></span></a>
            <?php } ?>    

            <?php if (!empty(get_theme_mod('social_links_dribbble'))) { ?>     
                <a class="dribbble" href="<?php echo get_theme_mod('social_links_dribbble'); ?>"><span class="fa fa-dribbble"></span></a>
            <?php } ?> 

        </div>

        <div class="col-md-4">
            
            <?php dynamic_sidebar('footer-1'); ?>
        </div>

        <div class="col-md-5 footer-contact-form">
            
            <?php dynamic_sidebar('footer-2'); ?>
            
            <!-- We can use it form for Contact form 7 plugin
            <h3>Quick contact us</h3>
            <div class="row ">
                <div class="col-md-12">
                    <div class="col-md-12 no-padding-right no-padding-left">
                        <div class="form-group" style="width: 50%;float: left;">
                            <input type="name" class="form-control" placeholder="Name*" required>
                        </div>
                        <div class="form-group" style="width: 50%;float: left;">
                            <input type="name" class="form-control" placeholder="Email*" style="border-left: 0;" required>
                        </div>
                    </div>
                    <textarea class="form-control text-uppercase" rows="8" id="comment" placeholder="Comment*"></textarea>
                    <button type="submit" class="btn btn-warning read-btn">Submit now</button>
                </div>
            </div>
            -->
        </div>
    </div>
</footer>



</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
